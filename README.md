k3s Hetzner
===========

How to build a kubernetes cluster in [Hetzner Cloud](https://www.hetzner.com/cloud).

## Why

Hetzner is a good cloud provider in Europe, this project show us how build a kubernetes cluster in Hetzner Cloud, using [k3s](https://k3s.io/), [cloud-init](https://cloudinit.readthedocs.io/en/latest/), [Terraform](https://www.terraform.io/) and [Ansible](https://docs.ansible.com/ansible/latest/index.html).

## Requirements.

- A Hetzner Cloud account.

## Packages used by the build process

- Cloud Init
- Terraform 0.13.x
- Ansible 2.9.x
- Python Hetzner Cloud module (`pip install hcloud`)

we get k3s 1.18.6+k3s1 version installed.

before execute the configuration, you need to setup your Hetzner Cloud API Key. you must setup this in your Hetzner account following [this  indications](https://docs.hetzner.cloud/#getting-started) and setting up the environment var `$HCLOUD_TOKEN`.

the Kubeconfig file will be setuop in `/tmp/kube_config.yaml` and you can do:

```
export KUBECONFIG=/tmp/kube_config.yaml
```

and after this, execute locally `kubectl` and `helm`.

Execution

```
export HCLOUD_TOKEN="HETZNER_CLOUD_API_TOKEN"
export KUBECONFIG=/tmp/kube_config.yaml
cp vars.auto.tfvars.example vars.auto.tfvars # and edit the new vars.auto.tfvars for your requirements, Hetzner datacenter, internal network, type and worker nodes quantity
terraform plan -out run.plan
terraform apply "run.plan"
ansible-playbook site.yml
kubectl get nodes
kubectl get pods -Aowide
```

