resource "hcloud_server" "master" {

  depends_on = [
    hcloud_network_subnet.kube_subnet,
  ]

  name        = var.master_name
  server_type = var.master_type
  image       = var.image
  location    = var.location
  user_data   = templatefile("files/cloud_config.yml",{
    key       = hcloud_ssh_key.key.public_key
  })
}

resource "hcloud_server_network" "master" {
  server_id   = hcloud_server.master.id
  subnet_id   = hcloud_network_subnet.kube_subnet.id
  ip          = var.master_ip
}

resource "hcloud_floating_ip" "master" {
  type      = "ipv4"
  server_id = hcloud_server.master.id
  count     = var.enable_servicelb? 1: 0
}

resource "null_resource" "master" {
  depends_on = [
    hcloud_server.master,
    hcloud_server_network.master,
    hcloud_floating_ip.master,
  ]
}
